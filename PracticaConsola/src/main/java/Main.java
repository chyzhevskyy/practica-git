
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author SEGUNDODAM
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) throws cdException, IOException, mkdirException, infoException {
        
        java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir"));
  
        Scanner escribir = new Scanner(System.in);
        String s = escribir.nextLine();
        if(s.equals("help")){
        
            help(s);
        
        }else if(s.equals("cd")){
            
            cd(s, ruta);
            
        }else if(s.equals("cd ..")){
            
            cd(s, ruta);
            
        }else if(s.equals("mkfile")){
            
            mkfile(s, ruta);
            
        }else if(s.equals("dir")){
            
            dir(s, ruta);
            
        }else if(s.equals("mkdir")){
            
            mkdir(s, ruta);
            
        }else if(s.equals("write")){
            
            write(s, ruta);
            
        }else if(s.equals("clear")){
            
            clear();
            
        }else if(s.equals("info")){
            
            info(s, ruta);
            
        }else if(s.equals("cat")){
            
            cat(s, ruta);
            
        }else if(s.equals("top")){
            
            top(s, ruta);
            
        }else if(s.equals("close")){
            
            close(s, ruta);
            
        }else if(s.equals("delete")){
            
            delete(s, ruta);
            
        }
        
    }// profe me han entrado ganas de tirarme por la ventana haciendo esto
    
    public static void help(String help){
        
        System.out.println("1. help-> Lista los comandos con una breve definición de lo que hacen.");
	System.out.println("2. cd-> Muestra el directorio actual. \n   "+ "2.1 [..]-> Accede al directorio padre. \n   "+ "2.2 [<nombreDirectorio>]-> Accede a un directorio dentro del directorio actual. \n   "+ "2.3 [<rutaAbsoluta>]-> Accedea la ruta absoluta del sistema.");
	System.out.println("3. mkdir <nombre_directorio>-> Crea un directorio en la ruta actual.");
	System.out.println("4. info <nombre>-> Muestra la información del elemento.");
	System.out.println("5. cat <nombreFichero>-> Muestra el contenido de un fichero");
	System.out.println("6. top <numeroLineas> <nombreFichero>-> Muestra las líneas especificadas de un fichero.");
	System.out.println("7. mkfile <nombreFichero> <texto>-> Crea un fichero con ese nombre y el contenido de texto.");
	System.out.println("8. write <nombreFichero> <texto>-> Añade 'texto' al final del fichero especificado.");
	System.out.println("9. dir-> Lista los archivos o directorios de la ruta actual. \n   "+ "9.1 [<nombreDirectorio>]-> Lista los archivos o directorios dentro de ese directorio. \n   "+ "9.2 [<rutaAbsoluta]-> Lista los archivos o directorios dentro de esa ruta.");
	System.out.println("10. delete <nombre>-> Borra el fichero, si es un directorio borra todo su contenido y a si mismo.");
	System.out.println("11 close-> Cierra el programa.");
	System.out.println("12 Clear-> Vacía la lista");
            
       }
    
    public static void cd(String cd, Path ruta) throws cdException {
        
                String[] posicion = cd.split(" ");
		String parte1 = posicion[0];
		
		for(int i=2;i<posicion.length;i++) {
			
                    if(posicion[i]!=null) {
				
                        throw new cdException("HUH algo va mal eh, revisalo bien");
				
                    }
		}
		
		if(parte1.equals(cd)) {
			
			System.out.println(ruta);
			
		} else {
			
			String parteCD = posicion[1];
			
			if(parteCD==null) {
				
				throw new cdException("Oye tu, que no puedes dejar esto en blanco");
				
			} else if(parteCD.equals("..")) {
				
				ruta=ruta.getParent();
				System.out.println(ruta);
					
			} else if(parteCD.contains(":")) {
				
				File d = new File(parteCD);
				String ruta2 =parteCD;
				ruta=Paths.get(ruta2);
				
				if(d.exists()) {
					
					System.out.println(ruta);
					
				} else {
					
					throw new cdException("Lo que buscas no existe,crealo primero merluzo");
					
				}
				
			} else {
					
				File f = new File(ruta.toString());
				
				String lista[]=f.list();
				
				int contador=0;
				
				for(int i=0;i<lista.length;i++) {
					
					if(lista[i].equals(parteCD)) {
						
						contador++;
						
					} 
				}
				 
				if(contador==1) {
					
					String ruta2 =ruta+"\\"+parteCD;
					ruta=Paths.get(ruta2);
					System.out.println(ruta);
					
				} else {
					
					throw new cdException("No existe dicho directorio");
					
				}
				
				
			} 
			
		}		
			
	}
    
    public static void dir(String dir, Path ruta) {
		
		File f = new File(ruta.toString());
		
		String lista[]=f.list();
		
		for(int i=0;i<lista.length;i++) {
			
			System.out.println(lista[i]);
			
		}
		
	}
    
    public static void mkdir(String mkdir, Path ruta) throws mkdirException {
        
            

			
		String[] partes = mkdir.split(" ");
		String parte2 = partes[1];
		
		for(int i=2;i<partes.length;i++) {
			
			if(partes[i]!=null) {
				
				throw new mkdirException("Algo anda mal... revisa el que");
				
			}
		} 
                    
                        String ruta2=ruta+"\\"+parte2;
			File directorio = new File(ruta2);
					
			if(directorio.exists()) {
						
				throw new mkdirException("Por desgracia ya existe uno directorio asi");
						
			} else {
				
				if(directorio.mkdirs()) {
				
					System.out.println("GG ya tienes tu directorio hecho");
					
				}
				
			}
	}
	
    public static void mkfile(String mkfile, Path ruta) throws IOException, mkdirException {

		
		String[] posicion = mkfile.split(" ");
		String parte2 = posicion[1];
		String parte3 = posicion[2];
		
		String ruta2=ruta+"\\"+parte2;
		File fichero = new File(ruta2);
		
		for(int i=3;i<posicion.length;i++) {
			
			if(posicion[i]!=null) {
				
				throw new IOException("Algo anda mal... revisa el que");
				
			}
		}
				
		if(fichero.exists()) {
					
			throw new mkdirException("Por desgracia ya existe uno fichero asi");
					
		} else {
			
			if(fichero.createNewFile()) {
			
				System.out.println("GG ya tienes tu fichero hecho");
				
			}
			
		}
			
			BufferedWriter bw=new BufferedWriter(new FileWriter(ruta2));
                        bw.write(parte3);
			bw.flush();
		
	}
    
    public static void write(String write, Path ruta) throws IOException, mkdirException {
		
		String[] partes = write.split(" ");
		String parte2 = partes[1];
		String parte3 = partes[2];
		
		String ruta2=ruta+"\\"+parte2;
		File fichero = new File(ruta2);
		
		for(int i=3;i<partes.length;i++) {
			
			if(partes[i]!=null) {
				
				throw new IOException("Revisa el formato.");
				
			}
		}
				
		if(fichero.exists()) {
					
			BufferedWriter bw=new BufferedWriter(new FileWriter(ruta2));
            bw.write(parte3);
			bw.flush();
					
		} else {
			
			throw new mkdirException("No existe dicho fichero.");
			
		}
		
	}
    
    public static void clear() throws IOException {
		
		      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      System.out.println("\n");
                      
		
	}

    public static void info(String info, Path ruta) throws infoException {
		
		String[] partes = info.split(" ");
		String parte2 = partes[1];

		for(int i=2;i<partes.length;i++) {
			
			if(partes[i]!=null) {
				
				throw new infoException("Revisa el formato.");
				
			}
		} 

		String ruta2=""+ruta+"\\"+parte2;
		ruta=Paths.get(ruta2);
		File directorio = new File(ruta2);
		
		if(!directorio.exists()) {
			
			throw new infoException("No existe dicho directorio");
			
		}
		
		int elementos=0;
		File f = new File(ruta2);
		String lista[]=f.list();
		
		for(int i=0;i<lista.length;i++) {
			
			elementos++;
			
		}
		
		System.out.println(ruta.getFileSystem());
		System.out.println(ruta.getParent());
		System.out.println(ruta.getRoot());
		System.out.println(elementos);
		System.out.println(directorio.getFreeSpace());
		System.out.println(directorio.getUsableSpace());
		System.out.println(directorio.getTotalSpace());
		
		
	}

    public static void cat(String cat, Path ruta) throws IOException {
		
		String[] partes = cat.split(" ");
		String parte2 = partes[1];
		
		for(int i=2;i<partes.length;i++) {
			
			if(partes[i]!=null) {
				
				throw new IOException("Algo anda mal... revisa el que");
				
			}
		}
		
		String ruta2=""+ruta+"\\"+parte2;
		
		File directorio = new File(ruta2);
		
		if(!directorio.exists()) {
			
			throw new IOException("Vaaaya no existe dicho fichero");
			
		}
		
		BufferedReader br=new BufferedReader(new FileReader(ruta2));
        String linea=br.readLine();
        while(linea!=null){
            System.out.println(linea);
            linea=br.readLine();
        }
		
	}
    
    public static void top(String top, Path ruta) throws IOException {
		
		String[] partes = top.split(" ");
		String parte2 = partes[1];
		String parte3 = partes[2];
		
		for(int i=3;i<partes.length;i++) {
			
			if(partes[i]!=null) {
				
				throw new IOException("Algo anda mal... revisa el que");
				
			}
		}
		
		int numlineas=Integer.parseInt(parte2);
		
		String ruta2=""+ruta+"\\"+parte3;
		
		File directorio = new File(ruta2);
		
		if(!directorio.exists()) {
			
			throw new IOException("Vaaaya no existe dicho fichero");
			
		}
		
		BufferedReader br=new BufferedReader(new FileReader(ruta2));
                String linea=br.readLine();
                
                int lineas=0;
                
                while(linea!=null){
                    if(lineas<numlineas) {
                        System.out.println(linea);
                        linea=br.readLine();
                        lineas++;
                        
                    }
                    
                }
                
	}
    
    public static void close(String close, Path ruta) throws IOException{
        
        System.exit(0);
        
    }

    public static boolean delete(String [] delete, Path ruta){
        
     boolean resultado = true;
     boolean existeFichero;
     if(delete.length == 2){
         
         
             
             try{
                 ruta= ruta.resolve(delete[1]);
         File fichero = new File (ruta.toString());
         existeFichero = fichero.exists();
         if(existeFichero){
                 
                 borrarDirectorio(fichero);
                 fichero.delete();
                  }else{
             resultado = false;
             System.out.println("Error: no existe");
         }

             }catch (SecurityException j){
                 j.printStackTrace();
             }
        
         
     }else{
         resultado = false;
         System.out.println("Error: el comando delete acepta 2 parametros");
     }
     return resultado;
        
    }

    private static void borrarDirectorio(File fichero) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void delete(String s, Path ruta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
